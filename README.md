wn_events MODULE
=====================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
------------

Used for sponsorship, donation, registration, crowdfunding, and ticket purchase.

REQUIREMENTS
------------
This module requires the following:
* Inline Entity Form (https://www.drupal.org/project/inline_entity_form)

Use Composer install brick/math and chillerlan/php-qrcode packages.
* composer require brick/math
* composer require chillerlan/php-qrcode

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

CONFIGURATION
-------------

* Configure wn_events MODULE SETTINGS
  Configure Will Nice Events in "/admin/config/system/wn_events".
  After the module is installed, the sponsorship payment related configuration will use the test account of the Will Nice unified payment platform. For more information, please go to: https://pay.will-nice.com/ Or send an email to Will Nice: service@will-nice.com

MAINTAINERS
-----------
This project has been developed by:
* Will Nice (Shenzhen) Technology Co., Ltd - https://www.will-nice.com/


wn_events 模块
=====================

文件内容
---------------------

* 介绍
* 依赖
* 安装
* 配置

介绍
------------

用于：赞助、捐赠、报名、众筹、购票。

依赖
------------
此模块需要依赖以下模块：
* Inline Entity Form(https://www.drupal.org/project/inline_entity_form)

使用Composer安装brick/math和chillerlan/phpqrcode包。
* composer require brick/math
* composer require chillerlan/php-qrcode

安装
------------

按照您通常安装已贡献的Drupal模块的方式进行安装。
请参阅：https://www.drupal.org/documentation/install/modules-themes/modules-8
以获取更多信息。

配置
-------------
* 配置wn_events模块
  在“/admin/config/system/wn_events”中配置Will Nice Events模块。
  模块安装后，赞助支付相关配置会使用Will Nice统一支付平台的测试账号。想了解更多相关信息，请前往：https://pay.will-nice.com/ ，或发送邮件至未来很美：service@will-nice.com

维护机构
-----------
本项目开发方：
* 未来很美（深圳）科技有限公司 - https://www.will-nice.com/
