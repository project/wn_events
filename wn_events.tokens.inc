<?php
/**
 * @file
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\Entity\Node;

/**
 * Implements hook_token_info().
 */
function wn_events_token_info() {
  $types['comment'] = [
    'name' => t("Will Nice Events Token"),
    'description' => t("Customized placeholders provided by the Will Nice Events Module"),
  ];
  $comment['field_wn_events_name'] = [
    'name' => t("Participants Name"),
    'description' => t("Name of participants"),
  ];
  $comment['field_wn_events_wechat'] = [
    'name' => t("Participants WeChat"),
    'description' => t("WeChat of participants"),
  ];
  $comment['field_wn_events_phone'] = [
    'name' => t("Participants Phone"),
    'description' => t("Phone number of participants"),
  ];
  $comment['field_wn_events_paid'] = [
    'name' => t("Events Paid"),
    'description' => t("0 | Waiting for payment, 1 | Successful payment, 2 | Payment failed, 3 | Refund in progress, 4 | Partial refund, 5 | Full refund"),
  ];
  $comment['comment_body'] = [
    'name' => t("Event Comment"),
    'description' => t("Event comment written by participants when paid for events."),
  ];
  $comment['field_wn_order_number'] = [
    'name' => t("Events Order Number"),
    'description' => t("Events order number paid by participants."),
  ];
  $comment['field_wn_events_money'] = [
    'name' => t("Events Money"),
    'description' => t("Amount of events paid for events content."),
  ];
  $comment['field_wn_hide_events_info'] = [
    'name' => t("If Hide Events Info"),
    'description' => t("If true hide events information."),
  ];
  return [
    'types' => $types,
    'tokens' => [
      'comment' => $comment,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function wn_events_tokens($type, $tokens, array $data, array $options,
  BubbleableMetadata $bubbleable_metadata
) {
  $replacements = [];
  if ($type == 'comment') {
    $comment = \Drupal::routeMatch()->getParameter('comment');
    if (!empty($comment)) {
      $nid = $comment->getCommentedEntityId();
      $node = Node::load($nid);
      $if_hide_cert_personal_info = FALSE;
      if (!$node->get('field_wn_hide_cert_personal_info')->isEmpty()) {
        $if_hide_cert_personal_info = $node->get('field_wn_hide_cert_personal_info')->value;
      }
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'created':
            $replacements[$original] = date('Y年m月d日', $comment->get('field_wn_events_time')->value);
            break;
          case 'cert_id':
            $replacements[$original] = date('ymd', $comment->get('field_wn_events_time')->value) . $comment->id();;
            break;
          case 'field_wn_events_name':
            $replacements[$original] = $comment->get('field_wn_events_name')->value;
            break;
          case 'field_wn_events_wechat':
            $wn_events_wechat = $comment->get('field_wn_events_wechat')->value;
            if ($if_hide_cert_personal_info && !empty($wn_events_wechat)) {
              $display_len = floor(strlen($wn_events_wechat) / 2);
              $asterisk_len = strlen($wn_events_wechat) - $display_len;
              $wn_events_wechat = substr($wn_events_wechat, 0, $display_len) . str_repeat('*', $asterisk_len);
            }
            $replacements[$original] = $wn_events_wechat;
            break;
          case 'field_wn_events_phone':
            $wn_events_phone = $comment->get('field_wn_events_phone')->value;
            if ($if_hide_cert_personal_info && !empty($wn_events_phone)) {
              $wn_events_phone = substr($wn_events_phone, 0, 3) . '****' . substr($wn_events_phone, 7);
            }
            $replacements[$original] = $wn_events_phone;
            break;
          case 'field_wn_events_paid':
            $replacements[$original] = $comment->get('field_wn_events_paid')->value;
            break;
          case 'comment_body':
            $replacements[$original] = $comment->get('comment_body')->value;
            break;
          case 'field_wn_order_number':
            $replacements[$original] = $comment->get('field_wn_order_number')->value;
            break;
          case 'field_wn_events_money':
            $replacements[$original] = $comment->get('field_wn_events_money')->value;
            break;
          case 'field_wn_hide_events_info':
            $replacements[$original] = $comment->get('field_wn_hide_events_info')->value;
            break;
        }
      }
    }
  }
  //Return the result so that we can now use the token.
  return $replacements;
}
