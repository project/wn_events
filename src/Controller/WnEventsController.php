<?php

/**
 *  WnEventsController
 *  by:LiJiacheng
 */

namespace Drupal\wn_events\Controller;

use chillerlan\QRCode\QRCode;
use Drupal\comment\CommentInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\wn_events\WnEventsPerformance;

class WnEventsController extends ControllerBase {

  public function showCertificate(NodeInterface $node, CommentInterface $comment = NULL) {
    $response = new Response();
    $performance_text = NULL;
    $performance_url = NULL;
    $field_wn_certificate_content = '';
    $certificate_status = 0;
    $field_wn_certificate_seal = NULL;
    $field_wn_cert_padding_block = 0;
    $field_wn_cert_padding_inline = 0;
    $field_wn_cert_background_img = NULL;
    $field_wn_certificate_seal_right = 0;
    $field_wn_cert_seal_bottom = 0;
    $wn_qrcode_query_image = NULL;
    $wn_qrcode_sponsor_image = NULL;
    $field_wn_qrcode_wh = 0;
    $field_wn_qrcode_display = 0;
    $field_wn_qrcode_left = 0;
    $field_wn_qrcode_bottom = 0;
    $destination = \Drupal::request()->query->get('destination');

    $field_is_need_perform = $node->get('field_is_need_perform')->value;
    $field_wn_certificate_content = $node->get('field_wn_certificate_content')->value;
    if (!empty($comment)) {
      if ($comment->isPublished()) {
        $certificate_status = 1;
        $token = \Drupal::token();
        $field_wn_certificate_content = $token->replace($field_wn_certificate_content);
        $qrcode_query_url = Url::fromRoute('wn_events.certificate', [
          'node' => $node->id(),
          'comment' => $comment->id(),
        ], [
          'absolute' => TRUE,
        ])->toString();
        $wn_qrcode_query_image = (new QRCode())->render($qrcode_query_url);
      }
      else {
        $certificate_status = -1;
      }
    }
    $field_wn_cert_padding_block = $node->get('field_wn_cert_padding_block')->value;
    $field_wn_cert_padding_inline = $node->get('field_wn_cert_padding_inline')->value;
    $field_wn_certificate_seal_right = $node->get('field_wn_certificate_seal_right')->value;
    $field_wn_cert_seal_bottom = $node->get('field_wn_cert_seal_bottom')->value;
    if (!$node->get('field_wn_certificate_seal')->isEmpty()) {
      $field_wn_certificate_seal = \Drupal::service('file_url_generator')
        ->generate($node->get('field_wn_certificate_seal')->entity->getFileUri())
        ->toString();
    }
    if (!$node->get('field_wn_cert_background_img')->isEmpty()) {
      $field_wn_cert_background_img = \Drupal::service('file_url_generator')
        ->generate($node->get('field_wn_cert_background_img')->entity->getFileUri())
        ->toString();
    }
    $sponsor_content_url = Url::fromRoute('entity.node.canonical', [
      'node' => $node->id(),
    ], [
      'absolute' => TRUE,
    ])->toString();
    $wn_qrcode_sponsor_image = (new QRCode())->render($sponsor_content_url);
    $field_wn_qrcode_wh = $node->get('field_wn_qrcode_wh')->value;
    $field_wn_qrcode_display = $node->get('field_wn_qrcode_display')->value;
    $field_wn_qrcode_space = $node->get('field_wn_qrcode_space')->value;
    $field_wn_qrcode_left = $node->get('field_wn_qrcode_left')->value;
    $field_wn_qrcode_bottom = $node->get('field_wn_qrcode_bottom')->value;
    $module_path = "/" . \Drupal::service("extension.path.resolver")
        ->getPath('module', 'wn_events');
    $wn_events_certificate_query_url = NULL;
    $wn_events_phone = \Drupal::request()->query->get('phone');
    $wn_events_certificate_number = \Drupal::request()->query->get('wn_events_certificate_number');
    if (!empty($wn_events_phone) || !empty($wn_events_certificate_number)) {
      $wn_events_certificate_query_url = Url::fromRoute('view.wn_events_certificate_query.page_1', [
        'node' => $node->id(),
      ], [
        'query' => [
          'phone' => $wn_events_phone,
          'wn_events_certificate_number' => $wn_events_certificate_number,
        ],
      ])->toString();

      $performance_text = WnEventsPerformance::getCommentPerformanceText($comment);
      $performance_url = Url::fromRoute('wn_events.performance_form', [
        'node' => $node->id(),
        'comment' => $comment->id(),
      ], [
        'query' => [
          'phone' => $wn_events_phone,
          'wn_events_certificate_number' => $wn_events_certificate_number,
          'from_cert' => 1,
        ],
      ])->toString();
    }
    else {
      if ($field_is_need_perform && WnEventsPerformance::hasPerformanceRole()) {
        $performance_text = WnEventsPerformance::getCommentPerformanceText($comment);
        $performance_url = Url::fromRoute('wn_events.performance_form', [
          'node' => $node->id(),
          'comment' => $comment->id(),
        ], [])->toString();
      }
    }
    $build = [
      '#theme' => 'wn_events_certificate',
      '#wn_destination' => $destination,
      '#wn_is_need_perform' => $field_is_need_perform ? '1' : '0',
      '#wn_performance_text' => $performance_text,
      '#wn_performance_url' => $performance_url,
      '#wn_content' => $field_wn_certificate_content,
      '#wn_certificate_status' => $certificate_status,
      '#wn_certificate_seal' => $field_wn_certificate_seal,
      '#wn_certificate_padding_block' => $field_wn_cert_padding_block,
      '#wn_certificate_padding_inline' => $field_wn_cert_padding_inline,
      '#wn_certificate_background_img' => $field_wn_cert_background_img,
      '#wn_certificate_seal_right' => $field_wn_certificate_seal_right,
      '#wn_certificate_seal_bottom' => $field_wn_cert_seal_bottom,
      '#wn_qrcode_query_image' => $wn_qrcode_query_image,
      '#wn_qrcode_sponsor_image' => $wn_qrcode_sponsor_image,
      '#wn_qrcode_wh' => $field_wn_qrcode_wh,
      '#wn_qrcode_display' => $field_wn_qrcode_display,
      '#wn_qrcode_space' => $field_wn_qrcode_space,
      '#wn_qrcode_left' => $field_wn_qrcode_left,
      '#wn_qrcode_bottom' => $field_wn_qrcode_bottom,
      '#wn_events_content_url' => Url::fromRoute('entity.node.canonical', [
        'node' => $node->id(),
      ], [])->toString(),
      '#wn_events_certificate_query_url' => $wn_events_certificate_query_url,
      '#wn_module_path' => $module_path,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $build['#attached']['library'][] = "wn_events/show_certificate";
    $build['#attached']['drupalSettings']['module_path'] = $module_path;
    $build['#attached']['drupalSettings']['wn_events_content_redirect'] = Url::fromRoute('entity.node.canonical', ['node' => $node->id()], [])
      ->toString();
    return $build;
  }

  public function getEventsFormPageTitle(NodeInterface $node) {
    $events_action_label = $node->get('field_wn_events_entry_button')->value;
    $suffix_text = $this->t('payment');
    return "{$events_action_label} {$suffix_text}";
  }

}
