<?php

/**
 * After the payment of the order on the unified platform is completed,
 * it will be returned to the merchant, where the subsequent business logic
 * related to the order is processed
 * 在统一平台订单付款完成后将返回商家，这里处理和订单相关的后续业务逻辑
 * by:yunke email:phpworld@qq.com
 */

namespace Drupal\wn_events\Controller\payment;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @package Drupal\wn_events\Controller
 */
class OrderProcess extends ControllerBase {

  //日志器
  protected $logger;

  public function __construct() {
    $this->logger = $this->getLogger("wn_events");
  }

  /**
   * Process asynchronous notification of payment status sent by the unified
   * platform 处理统一平台发来的支付状态异步通知
   *
   * @param null $orderNumber
   *
   * @return string[]|\Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function index($orderNumber = NULL) {
    $storager = \Drupal::entityTypeManager()->getStorage("comment");
    $orderIds = $storager->getQuery('AND')
      ->accessCheck(TRUE)
      ->condition("comment_type", "wn_events_comment", '=')
      ->condition("field_wn_order_number", $orderNumber, '=')
      ->execute();
    if (empty($orderIds)) {
      $warning_message = $this->t('The order does not exist on the payment return page')
        ->render();
      $this->logger->warning($warning_message);
      return [
        '#markup' => 'Project order does not exist',
      ];
    }
    $commentEntity = $storager->load(array_shift($orderIds));
    $orderPayState = $commentEntity->get('field_wn_events_paid')->value;
    if ($orderPayState != YK_ORDER_STATE_SUCCESS) {
      //Order payment has not yet been successfully initiated for proactive inquiry
      //订单付款尚未成功 开始主动查询
      $order = [];
      $order['order_number'] = $orderNumber;
      $sdk = \Drupal::service('wn_events_paysdk.pay');
      $result = $sdk->query($order);
      if ($result['code'] >= 4000) {
        //Query failed
        //查询失败
        $warning_message = $this->t('Payment active query failed:@msg', [
          '@msg' => $result['msg'],
        ])->render();
        $this->logger->warning($warning_message);
      }
      else {
        if (isset($result['order_state']) && $result['order_state'] == YK_ORDER_STATE_SUCCESS) {
          $commentEntity->set('status', TRUE);
        }
        $commentEntity->set('field_wn_events_paid', $result['order_state']);
        $commentEntity->save();
      }
    }
    return $this->response($commentEntity);
  }

  public function response($commentEntity) {
    $orderPayState = $commentEntity->get('field_wn_events_paid')->value;
    $node_id = $commentEntity->getCommentedEntityId();
    if ($orderPayState != YK_ORDER_STATE_SUCCESS) {
      //Order payment has not been successful yet
      //订单付款尚未成功
      $warning_message = $this->t('Payment has not been received yet. Please wait or try again')
        ->render();
      $this->messenger()->addWarning($warning_message);
      $url = Url::fromRoute('wn_events.wn_events_form', [
        'node' => $node_id,
      ], [])->toString();
      return new RedirectResponse($url);
    }
    else {
      //Successfully paid
      //已经付款成功
      $success_message = $this->t('Successfully paid!')->render();
      $this->messenger()->addStatus($success_message);
      $commentEntity->set('field_wn_events_paid', TRUE);
      $url = Url::fromRoute('wn_events.certificate', [
        'node' => $node_id,
        'comment' => $commentEntity->id(),
      ], [])->toString();
      return new RedirectResponse($url);
    }
  }

}

