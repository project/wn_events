<?php

/**
 *  Receive asynchronous order notifications from the unified platform
 *  接收来自统一平台的异步订单通知
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\wn_events\Controller\payment;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package Drupal\wn_events\Controller
 */
class OrderNotify extends ControllerBase {

  //日志器
  protected $logger;

  public function __construct() {
    $this->logger = $this->getLogger("wn_events");
  }

  /**
   * Process asynchronous notification of payment status sent by the unified platform
   * 处理统一平台发来的支付状态异步通知
   *
   * @param null $orderNumber
   */
  public function index($orderNumber = NULL) {
    //Calling service to verify signature
    //调用服务验证签名
    $data = file_get_contents('php://input');
    try {
      $data = json_decode($data);
      $data = (array) $data;
    }
    catch (\Exception $e) {
      $warning_message = $this->t('Payment asynchronous notification data deserialization exception')->render();
      $this->logger->warning($warning_message);
      return $this->response();
    }
    $sdk = \Drupal::service('wn_events_paysdk.pay');
    $verifyResult = $sdk->verifySign($data);
    if (!$verifyResult) {
      $warning_message = $this->t('Payment asynchronous notification data encryption verification failed')->render();
      $this->logger->warning($warning_message);
      return $this->response();
    }
    if (empty($orderNumber) || empty($data['order_number']) || $orderNumber != $data['order_number']) {
      $this->logger->warning('Incorrect payment asynchronous notification data');
      return $this->response();
    }
    $storager = \Drupal::entityTypeManager()->getStorage("comment");
    $orderIds = $storager->getQuery('AND')
      ->accessCheck(TRUE)
      ->condition("comment_type", "wn_events_comment", '=')
      ->condition("field_wn_order_number", $orderNumber, '=')
      ->execute();
    if (empty($orderIds)) {
      $warning_message = $this->t('Payment asynchronous notification order does not exist')->render();
      $this->logger->warning($warning_message);
      return $this->response();
    }
    $commentEntity = $storager->load(array_shift($orderIds));
    $orderPayState = $commentEntity->get('field_wn_events_paid')->value;
    if ($orderPayState == YK_ORDER_STATE_SUCCESS) {
      //The order has already been processed
      //订单已经处理过了
      return $this->response();
    }
    if ($data['order_state'] == YK_ORDER_STATE_SUCCESS) {
      //Successfully updated order for payment
      //付款成功 更新订单
      $commentEntity->set('status', TRUE);
      $hook_data = [
        'content_id' => $commentEntity->getCommentedEntityId(),
        'comment_id' => $commentEntity->id(),
        'user' => [
          'name' => $commentEntity->get('field_wn_events_name')->value,
          'wechat' => $commentEntity->get('field_wn_events_wechat')->value,
          'phone' => $commentEntity->get('field_wn_events_phone')->value,
        ],
        'amount' => $commentEntity->get('field_wn_events_money')->value,
        'time' => $commentEntity->get('field_wn_events_time')->value,
      ];
      hook_wn_events_payment_success($hook_data);
    }
    $commentEntity->set('field_wn_events_paid', $data['order_state']);
    $commentEntity->save();
    return $this->response();
  }

  public function response() {
    return new Response('success');
  }

}

