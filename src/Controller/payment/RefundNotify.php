<?php

/**
 *  Receive asynchronous refund notifications from the unified platform
 *  接收来自统一平台的退款异步通知
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\wn_events\Controller\payment;

use Brick\Math\BigDecimal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package Drupal\wn_events\Controller
 */
class RefundNotify extends ControllerBase {

  public function __construct() {}

  /**
   * Process asynchronous refund notifications sent by the unified platform
   * 处理统一平台发来的退款异步通知
   *
   * @param \Drupal\node\NodeInterface|null $node
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function index() {
    //Check the order number and refund note number
    //Verify signature
    //Change Order Status
    //Process subsequent business logic related to orders, but this can also be handled in the query refund logic
    //检查订单号、退款单号
    //验证签名
    //变更订单状态
    //处理和订单相关的后续业务逻辑，但这也可以在查询退款逻辑中处理
    $nid = \Drupal::request()->query->get('nid');
    $node = Node::load($nid);
    if (!empty($node)) {
      if (!$node->get('field_wn_refund_state')
          ->isEmpty() && $node->get('field_wn_refund_state')->value != YK_ORDER_REFUND_STATE_SUCCESS) {
        $node->set('field_wn_success_time', time());
        $node->set('field_wn_refund_state', YK_ORDER_REFUND_STATE_SUCCESS);
        $node->save();
        if (!$node->get('field_wn_order_number')->isEmpty()) {
          $order_number = $node->get('field_wn_order_number')->value;
          $comments = \Drupal::entityTypeManager()
            ->getStorage('comment')
            ->loadByProperties([
              'comment_type' => 'wn_events_comment',
              'field_wn_order_number' => $order_number,
            ]);
          foreach ($comments as $comment) {
            $field_wn_events_money = $comment->get('field_wn_events_money')->value;
            $sponsorship_money = BigDecimal::of($field_wn_events_money);
            $field_wn_events_money = $node->get('field_wn_events_money')->value;
            $minus_money = BigDecimal::of($field_wn_events_money);
            $sponsorship_remaining_money = BigDecimal::of($sponsorship_money->minus($minus_money));
            if ($sponsorship_remaining_money->compareTo(BigDecimal::zero()) < 1) {
              $comment->set('status', FALSE);
              $comment->set('field_wn_events_paid', YK_ORDER_STATE_REFUND_FULL);
            }
            else {
              $comment->set('field_wn_events_paid', YK_ORDER_STATE_REFUND_PART);
            }
            $comment->set('field_wn_events_money', $sponsorship_remaining_money);
            $comment->save();
            $hook_data = [
              'content_id' => $comment->getCommentedEntityId(),
              'comment_id' => $comment->id(),
              'user' => [
                'name' => $comment->get('field_wn_events_name')->value,
                'wechat' => $comment->get('field_wn_events_wechat')->value,
                'phone' => $comment->get('field_wn_events_phone')->value,
              ],
              'amount' => $comment->get('field_wn_events_money')->value,
              'time' => $comment->get('field_wn_events_time')->value,
            ];
            hook_wn_events_refund_success($hook_data);
          }
        }
      }
    }
    return new Response('success');
  }

}

