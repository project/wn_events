<?php
/**
 * 统一支付接口相关操作
 * 开发者: yunke
 * Email: phpworld@qq.com
 */

namespace Drupal\wn_events;

use \Drupal\Core\Config\ConfigFactoryInterface;
use \Drupal\Core\Logger\LoggerChannelFactoryInterface;
use \Drupal\wn_events\Util\AES;
use GuzzleHttp\Exception\RequestException;


class Pay {

  //统一支付接口配置
  protected $config;

  //日志记录器
  protected $logger;


  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $channelFactory) {
    $this->config = $configFactory->get('wn_events.settings');
    $this->logger = $channelFactory->get('wn_events');
  }


  public function order($order = []) {
    if (!isset($order['user_id'])) {
      $order['user_id'] = $this->config->get('userID');
    }
    $this->addSign($order);

    //已经准备好数据并签名，现在发送到统一付款平台，换取跳转链接
    $client = \Drupal::httpClient();
    $clientURL = $this->config->get('api.orderURL');
    $HTTPOptions = [
      'json'    => $order,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $jsonArr = json_decode($resp->getBody()->getContents()); //响应数组
      if ($jsonArr instanceof \stdClass) {
        $jsonArr = (array) $jsonArr;
      }
      if (!is_array($jsonArr)) {
        return ['code' => 4001, 'msg' => '参数错误']; //响应参数错误
      }
      if (!$this->verifySign($jsonArr)) {
        return ['code' => 4002, 'msg' => '签名验证错误']; //响应签名错误
      }
      return $jsonArr; //其值类似['code' => 2000, 'msg' => 'ok','url'=>'http://www.will-nice.com']
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "付款请求失败，响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      return ['code' => 5000, 'msg' => $msg]; //返回响应失败
    }

  }

  /**
   * 向统一平台发起订单查询 调用前应该先验证参数
   *
   * @param array $order
   *
   * @return array 响应状态或订单状态
   */
  public function query($order = []) {
    /**
     * 传入以下参数（见接口文档）：
     * $order['order_number'] 必须的订单号
     * $order['user_id'] 可选的用户id，如不传入采用系统默认
     */

    if (!isset($order['user_id'])) {
      $order['user_id'] = $this->config->get('userID');
    }
    $this->addSign($order);

    //已经准备好数据并签名，现在发送到统一付款平台，获取订单状态
    $client = \Drupal::httpClient();
    $clientURL = $this->config->get('api.queryURL');
    $HTTPOptions = [
      'json'    => $order,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $jsonArr = json_decode($resp->getBody()->getContents()); //响应数组
      if ($jsonArr instanceof \stdClass) {
        $jsonArr = (array) $jsonArr;
      }
      if (!is_array($jsonArr)) {
        return ['code' => 4001, 'msg' => '参数错误']; //响应参数错误
      }
      if (!$this->verifySign($jsonArr)) {
        return ['code' => 4002, 'msg' => '响应签名验证错误']; //响应签名错误
      }
      return $jsonArr;
      //其值类似['code' => 2000, 'msg' => 'ok', ...]
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "订单查询请求失败，响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      return ['code' => 5000, 'msg' => $msg]; //返回响应失败
    }

  }

  /**
   * 退款操作  调用前应先验证参数
   *
   * @param array $order
   *
   * @return array|mixed|\stdClass
   */
  public function refund($order = []) {
    /**
     * 传入以下参数（见接口文档）：
     * $order['user_id'] 可选的用户id，如不传入采用系统默认
     * $order['order_number'] 订单号
     * $order['refund_number'] 可选的退款单号
     * $order['amount'] 退款金额
     * $order['reason'] 退款原因
     * $order['notify_url'] 退款异步通知链接
     */

    if (!isset($order['user_id'])) {
      $order['user_id'] = $this->config->get('userID');
    }
    $this->addSign($order);

    $client = \Drupal::httpClient();
    $clientURL = $this->config->get('api.refundURL');
    $HTTPOptions = [
      'json'    => $order,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $jsonArr = json_decode($resp->getBody()->getContents()); //响应数组
      if ($jsonArr instanceof \stdClass) {
        $jsonArr = (array) $jsonArr;
      }
      if (!is_array($jsonArr)) {
        return ['code' => 4001, 'msg' => '参数错误']; //响应参数错误
      }
      if (!$this->verifySign($jsonArr)) {
        return ['code' => 4002, 'msg' => '响应签名验证错误']; //响应签名错误
      }
      return $jsonArr;
      //其值类似['code' => 2000, 'msg' => 'ok', ...]
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "订单退款请求失败，响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      return ['code' => 5000, 'msg' => $msg]; //返回响应失败
    }

  }

  /**
   * 查询退款  调用前应先验证参数
   *
   * @param array $order
   *
   * @return array|mixed|\stdClass
   */
  public function queryRefund($order = []) {
    /**
     * 传入以下参数（见接口文档）：
     * $order['user_id'] 可选的用户id，如不传入采用系统默认
     * $order['order_number'] 订单号
     * $order['refund_number'] 可选的退款单号
     */

    if (!isset($order['user_id'])) {
      $order['user_id'] = $this->config->get('userID');
    }
    $this->addSign($order);

    //已经准备好数据并签名，现在发送到统一平台，获取退款状态
    $client = \Drupal::httpClient();
    $clientURL = $this->config->get('api.queryRefundURL');
    $HTTPOptions = [
      'json'    => $order,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $jsonArr = json_decode($resp->getBody()->getContents()); //响应数组
      if ($jsonArr instanceof \stdClass) {
        $jsonArr = (array) $jsonArr;
      }
      if (!is_array($jsonArr)) {
        return ['code' => 4001, 'msg' => '参数错误']; //响应参数错误
      }
      if (!$this->verifySign($jsonArr)) {
        return ['code' => 4002, 'msg' => '响应签名验证错误']; //响应签名错误
      }
      return $jsonArr;
      //其值类似['code' => 2000, 'msg' => 'ok', ...]
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "订单退款查询请求失败，响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      return ['code' => 5000, 'msg' => $msg]; //返回响应失败
    }

  }

  /**
   * 得到签名串
   *
   * @param array  $data 发送到统一平台的订单信息数组
   * @param string $key  加密密钥 一般不用传递
   *
   * @return string
   */
  public function getSign($data = [], $key = '') {
    unset($data['sign']);
    ksort($data);
    if (empty($key)) {
      $key = $this->config->get('key');
    }
    $str = implode('', $data); //这里用空连接符也意在避免参数存在但无值时签名不同的问题
    $str = (new AES())->aesEncrypt($str, $key);
    $sign = hash('sha256', $str);
    return $sign;
  }

  /**
   * 给接口数据签名
   *
   * @param array  $data
   * @param string $key
   *
   * @return array
   */
  public function addSign(array &$data = [], $key = '') {
    if (empty($key)) {
      $key = $this->config->get('key');
    }
    $data['sign'] = $this->getSign($data, $key);
    return $data;
  }

  /**
   * 验证签名
   *
   * @param array  $data
   * @param string $key
   *
   * @return bool 签名验证失败返回false
   */
  public function verifySign(array $data = [], $key = '') {
    if (empty($key)) {
      $key = $this->config->get('key');
    }
    if (!isset($data['sign']) || $data['sign'] != $this->getSign($data, $key)) {
      return FALSE;
    }
    return TRUE;
  }


  /**
   * 验证接口参数 在调用本类其他面向接口的方法前均应该调用本方法进行参数验证
   *
   * @param $parameters array 参数
   *
   * @param $type       string 参数类型  order:下单接口参数  query:查询接口参数
   *
   * @return bool|string 验证通过返回true，否则返回失败的原因字符串
   */
  public function verifyParameters($parameters, $type = 'order') {
    //验证通过返回true 失败返回字符串原因表示

    if ($type == 'order') {
      //验证订单描述
      if (!isset($parameters['description'])) {
        return 'description:订单描述缺失';
      }
      if (!is_string($parameters['description'])) {
        return 'description:订单描述不是字符串';
      }
      $description = strlen($parameters['description']);
      if ($description > 127) {
        return 'description:订单描述字符数超过限制';
      }
      $limitStr = '/=&';
      if (strpbrk($parameters['description'], $limitStr) !== FALSE) {
        return 'description:订单描述含有限制字符，这些字符不能使用' . $limitStr;
      }

      //验证订单金额
      if (!isset($parameters['total'])) {
        return 'total:订单金额缺失';
      }
      if (!is_numeric($parameters['total'])) {
        return 'total:订单金额不是数字';
      }
      if (bccomp($parameters['total'], '100000000.00', 2) == 1 || bccomp($parameters['total'], '0.01', 2) == -1) {
        return 'total:订单金额应该在0.01~100000000.00之间';
      }
      if (strlen(substr(strrchr((string) $parameters['total'], "."), 1)) > 2) {
        return 'total:订单金额最大精确到小数点后两位';
      }

      //验证订单超时时间
      if (!isset($parameters['timeout_express'])) {
        return 'timeout_express:订单关闭时间缺失';
      }
      if (!is_numeric($parameters['timeout_express'])) {
        return 'timeout_express:订单超时时间应是一个UNIX时间戳';
      }
      $timeRange = $parameters['timeout_express'] - time();
      if ($timeRange < 60 || $timeRange > 1296000) {
        return 'timeout_express:订单有效时间应在1分钟到15天内';
      }

      //验证订单号
      if (!isset($parameters['order_number'])) {
        return 'order_number:订单号缺失';
      }
      $pattern = '/^[a-z0-9A-Z_]{6,32}$/';
      if (!preg_match($pattern, $parameters['order_number'])) {
        return 'order_number:订单号仅能包含大小写字母、数字、下划线，且长度大于等于6，小于等于32';
      }

      //验证订单返回链接
      if (!isset($parameters['return_url'])) {
        return 'return_url:订单返回链接缺失';
      }
      $pattern = "/^http[s]?:\/\/[\w.]+/i";
      if (!preg_match($pattern, $parameters['return_url'])) {
        return 'return_url:订单返回链接必须是一个绝对URL地址';
      }
      if (strlen($parameters['return_url']) > 256) {
        return 'return_url:订单返回链接太长，超过了256个字符';
      }

      //验证订单通知链接
      if (isset($parameters['notify_url'])) {
        $pattern = "/^http[s]?:\/\/[\w.]+/i";
        if (!preg_match($pattern, $parameters['notify_url'])) {
          return 'notify_url:订单通知链接必须是一个URL绝对地址';
        }
        if (strlen($parameters['notify_url']) > 256) {
          return 'notify_url:订单通知链接太长，超过了256个字符';
        }
      }

      //验证订单附加数据
      if (isset($parameters['attach'])) {
        if (!is_string($parameters['attach'])) {
          return 'attach:订单附加数据不是字符串，推荐用json编码';
        }
        if (strlen($parameters['attach']) > 128) {
          return 'attach:订单附加数据太长，超过了128个字符';
        }
      }
      // 验证通过
      return TRUE;
    }
    elseif ($type == 'query') {
      //验证订单号
      if (!isset($parameters['order_number'])) {
        return 'order_number:订单号缺失';
      }
      $pattern = '/^[a-z0-9A-Z_]{6,32}$/';
      if (!preg_match($pattern, $parameters['order_number'])) {
        return 'order_number:订单号仅能包含大小写字母、数字、下划线，且长度大于等于6，小于等于32';
      }
      // 验证通过
      return TRUE;
    }
    elseif ($type == 'refund') {
      //验证订单号
      if (!isset($parameters['order_number'])) {
        return 'order_number:订单号缺失';
      }
      $pattern = '/^[a-z0-9A-Z_]{6,32}$/';
      if (!preg_match($pattern, $parameters['order_number'])) {
        return 'order_number:订单号仅能包含大小写字母、数字、下划线，且长度大于等于6，小于等于32';
      }

      //验证退款单号
      if (isset($parameters['refund_number'])) {
        $pattern = '/^[a-z0-9A-Z_]{6,32}$/';
        if (!preg_match($pattern, $parameters['refund_number'])) {
          return 'refund_number:退款单号仅能包含大小写字母、数字、下划线，且长度大于等于6，小于等于32';
        }
      }

      //验证退款金额
      if (!isset($parameters['amount'])) {
        return 'amount:退款金额缺失';
      }
      if (!is_numeric($parameters['amount'])) {
        return 'amount:退款金额不是数字';
      }
      if (bccomp($parameters['amount'], '100000000.00', 2) == 1 || bccomp($parameters['amount'], '0.01', 2) == -1) {
        return 'total:订单金额应该在0.01~100000000.00之间';
      }
      if (strlen(substr(strrchr((string) $parameters['amount'], "."), 1)) > 2) {
        return 'total:订单金额最大精确到小数点后两位';
      }

      //验证退款原因
      if (isset($parameters['reason'])) {
        $reason = strlen($parameters['reason']);
        if ($reason > 80) {
          return 'reason:退款原因描述字符数超过限制';
        }
      }

      //验证退款异步通知链接
      if (isset($parameters['notify_url'])) {
        $pattern = "/^http[s]?:\/\/[\w.]+/i";
        if (!preg_match($pattern, $parameters['notify_url'])) {
          return 'notify_url:退款通知链接必须是一个URL绝对地址';
        }
        if (strlen($parameters['notify_url']) > 256) {
          return 'notify_url:退款通知链接太长，超过了256个字符';
        }
      }
      // 验证通过
      return TRUE;
    }
    elseif ($type == 'query_refund') {
      //验证订单号
      if (!isset($parameters['order_number'])) {
        return 'order_number:订单号缺失';
      }
      $pattern = '/^[a-z0-9A-Z_]{6,32}$/';
      if (!preg_match($pattern, $parameters['order_number'])) {
        return 'order_number:订单号仅能包含大小写字母、数字、下划线，且长度大于等于6，小于等于32';
      }

      //验证退款单号
      if (isset($parameters['refund_number'])) {
        $pattern = '/^[a-z0-9A-Z_]{6,32}$/';
        if (!preg_match($pattern, $parameters['refund_number'])) {
          return 'refund_number:退款单号仅能包含大小写字母、数字、下划线，且长度大于等于6，小于等于32';
        }
      }
      // 验证通过
      return TRUE;
    }

  }

}
