<?php

namespace Drupal\wn_events\Util;

/**
 * AES加密类, 加密算法aes-128-cbc，分组长度128比特  加密模式为CBC
 * author: yunke
 * email: phpworld@qq.com
 */
class AES{

  /**
   * 使用AES加密算法加密文本
   *
   * @param $plainText String 明文 不限制位数 即便是空字符""都行
   * @param $key       String 对称密钥 不限制位数 即便是空字符""都行，但建议在16个字符
   *
   * @return string base64转码后的密文
   */
  public function aesEncrypt($plainText, $key) {
    $iv = substr(hash('sha256', $key), 0, 16); //产生初始化加密向量
    return  openssl_encrypt($plainText, 'aes-128-cbc', $key, 0, $iv); //也可用aes-128-ctr
  }

  /**
   * AES解密
   *
   * @param $cipherText String 密文
   * @param $key        String 对称密钥
   *
   * @return string
   */
  public function aesDecrypt($cipherText, $key) {
    $iv = substr(hash('sha256', $key), 0, 16);
    return openssl_decrypt($cipherText, 'aes-128-cbc', $key, 0, $iv);
  }

}
