<?php

namespace Drupal\wn_events;

use Drupal\comment\CommentInterface;
use Drupal\node\Entity\Node;

/**
 * 开发公司：未来很美（深圳）科技有限公司 (www.will-nice.com)
 * by:LiJiacheng
 * 核销内容工具类
 */
class WnEventsPerformance {

  public static function getPerformanceText($performance_state = 0) {
    switch ($performance_state) {
      case PERFORMANCE_STATE_PERFORMING:
        $performance_state_text = t('PERFORMING(@time)', [
          '@time' => date('Y-m-d H:i:s'),
        ]);
        break;
      case PERFORMANCE_STATE_PERFORMED:
        $performance_state_text = t('PERFORMED(@time)', [
          '@time' => date('Y-m-d H:i:s'),
        ]);
        break;
      case PERFORMANCE_STATE_UNPERFORMED:
      default:
        $performance_state_text = t('UNPERFORMED(@time)', [
          '@time' => date('Y-m-d H:i:s'),
        ]);
        break;
    }
    return $performance_state_text;
  }

  public static function getCommentPerformanceText(CommentInterface $comment) {
    $performances = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'field_related_cert' => $comment->id(),
      ]);
    $performance = reset($performances);
    $performance_state_text = NULL;
    if ($performance) {
      $arr_state_text = $performance->get('field_time')->getValue();
      $arr_state_text = end($arr_state_text);
      if ($arr_state_text) {
        $performance_state_text = $arr_state_text['value'];
      }
    }
    return $performance_state_text;
  }

  public static function createCommentPerformanceNode(CommentInterface $comment) {
    $performance = Node::create([
      'title' => $comment->getSubject(),
      'type' => 'wn_performance',
      'field_state' => PERFORMANCE_STATE_UNPERFORMED,
      'field_time' => [self::getPerformanceText(PERFORMANCE_STATE_UNPERFORMED)],
      'field_related_cert' => $comment->id(),
    ]);
    $performance->save();
    return $performance;
  }

  public static function getCommentPerformanceNode(CommentInterface $comment) {
    $performances = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'field_related_cert' => $comment->id(),
      ]);
    $performance = reset($performances);
    if (!$performance) {
      $performance = self::createCommentPerformanceNode($comment);
    }
    return $performance;
  }

  public static function getPerformanceStateOptions() {
    return [
      0 => t('UNPERFORMED'),
      1 => t('PERFORMING'),
      2 => t('PERFORMED'),
    ];
  }

  public static function getCertificateNumber(CommentInterface $comment) {
    $time = $comment->getCreatedTime();
    $strtime = date('ymd', $time);
    return $strtime . $comment->id();
  }

  public static function hasPerformanceRole(){
    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    $allowed_roles = [
      'administrator',
      'wn_verification_administrator',
    ];
    if (array_intersect($allowed_roles, $roles)) {
      return TRUE;
    }
    return FALSE;
  }

}
