<?php
/**
 * Merchant order inquiry and refund
 * 商家订单查询退款
 *
 */

namespace Drupal\wn_events\Form\payment;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class QueryRefundForm extends FormBase {

  public function getFormId() {
    return 'wn_events_query_refund_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['order_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order No.'),
      '#description' => $this->t('Please enter the order number you submitted in the payment presentation'),
      '#maxlength' => 32,
      //'#pattern'     => '^[0-9a-zA-Z_]{6,32}$',
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['refund_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refund note number'),
      '#description' => $this->t('Please enter the refund number you entered on the refund presentation page'),
      '#maxlength' => 32,
      //'#pattern'     => '^[0-9a-zA-Z_]{6,32}$',
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Query refund'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::refund',
        'wrapper' => 'refund-result-wrapper',
        'prevent' => 'click',
        'method' => 'html',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Querying refund...'),
        ],
      ],
    ];
    $form['content_one'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('After querying, the refund result will be displayed here'),
      '#attributes' => ['id' => 'refund-result-wrapper'],
    ];
    return $form;
  }

  public function refund(array &$form, FormStateInterface $form_state) {
    $result = $form_state->get('result');
    unset($result['sign']);
    $title = $this->t('Refund query submitted');
    if (isset($result['refund_state'])) {
      $state = [
        0 => $this->t('Waiting for refund'),
        1 => $this->t('Refund successful'),
        2 => $this->t('Refund failed'),
      ];
      $title = $this->t('Refund status:')
          ->render() . '<strong>' . $state[(int) $result['refund_state']] . '</strong>';
    }
    $return = [];
    $return['refund_info'] = [
      '#type' => 'details',
      '#title' => $title,
      '#open' => TRUE,
    ];
    $data = $this->t('Unified platform returns data:')
        ->render() . '<pre>' . print_r($result, TRUE) . "</pre>";
    $return['refund_info']['data'] = ['#markup' => $data];
    return $return;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['order_number'] = trim($form_state->getValue('order_number'));
    $order['refund_number'] = trim($form_state->getValue('refund_number'));
    $sdk = \Drupal::service('wn_events_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order, 'query_refund');
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    $result = $sdk->queryRefund($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //In the AJAX submission of the form, after the validator passes, the submitter will execute it, and finally execute the Ajax callback
    //表单的AJAX提交中，验证器通过后提交器会执行 ，最后再执行ajax回调
  }

}
