<?php
/**
 * Merchant order refund
 * 商家订单退款
 *
 */

namespace Drupal\wn_events\Form\payment;

use Brick\Math\BigDecimal;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RefundForm extends FormBase {

  /**
   * The node storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * @var \Drupal\comment\CommentInterface
   */
  protected $comment;

  /**
   * @var string
   */
  protected $refundNum;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('node'),
      $container->get('current_route_match'),
    );
  }

  /**
   * Creates a EventsForm instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $comment_storage
   *   The comment storage handler.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The currently active route match object.
   */
  public function __construct(
    EntityStorageInterface $node_storage,
    RouteMatchInterface $current_route_match
  ) {
    $this->nodeStorage = $node_storage;
    $this->currentRouteMatch = $current_route_match;
    $this->comment = $this->currentRouteMatch->getParameter('comment');
    $this->refund_number = 'wn_event_' . date('ymdHis') . mt_rand(10000000, 99999999);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wn_events_refund_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $order_number = $this->comment->get('field_wn_order_number')->value;
    $order_number = empty($order_number) ? '' : $order_number;

    //Calculate the maximum amount that can be refunded for the refund order number
    //计算退款单号可退最大金额
    $max_refund_amount = $this->comment->get('field_wn_events_money')->value;
    $max_refund_money = BigDecimal::of($max_refund_amount);

    $form['order_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order No.'),
      '#description' => $this->t('Please enter the order number you submitted in the payment presentation'),
      '#maxlength' => 32,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
        'disabled' => 'disabled',
      ],
      '#default_value' => $order_number,
    ];
    $form['refund_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refund note number'),
      '#description' => $this->t('An order can be refunded multiple times to facilitate partial product refunds or user compensation. Each refund is uniquely identified by a refund number. The same refund number refers to the same refund request, and the input format is the same as the order number. Please use different refund numbers for different batches'),
      '#maxlength' => 32,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#default_value' => $this->refund_number,
    ];
    $form['amount'] = [
      '#type' => 'number',
      '#title' => $this->t('refund amount'),
      '#description' => $this->t('Unit: yuan, with a value range of 0.01~100000000.00. If the value is greater than 0, it is less than or equal to the total order amount. If multiple refunds are made, the total refund amount cannot be greater than the total order payment amount'),
      '#required' => TRUE,
      '#min' => 0.00,
      '#max' => $max_refund_money->toFloat(),
      '#step' => 0.01,
      '#field_suffix' => $this->t('yuan'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#default_value' => $max_refund_money->toFloat(),
    ];
    $form['reason'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reason for refund'),
      '#description' => $this->t('Optional. If passed in, the refund reason will be reflected in the refund message sent to the user, within 80 characters'),
      '#maxlength' => 80,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    //Asynchronous notification link for refund status
    //退款状态异步通知链接
    $route_parameters = [];
    $options = ['absolute' => TRUE,];
    $notify_url = new Url('wn_events.refund.notify', $route_parameters, $options);
    $form['notify_url'] = [
      '#type' => 'value',
      '#value' => $notify_url->toString(FALSE),
      //Asynchronous refund notification link, which will be used by the unified cashier platform to send a message to this system regarding the success of the refund
      //In this address, you should handle the order change logic or handle it after actively checking for refunds
      //String [1256] must be a directly accessible URL absolute address
      //Internal information is not displayed to users
      //退款异步通知链接，统一收银平台将采用该链接向本系统发送退款是否成功的消息
      //在该地址中你应该处理订单变更逻辑  或者在主动查询退款后处理
      //string[1,256] 必须为直接可访问的URL绝对地址
      //内部信息不展示给用户
    ];
    $form['actions']['#type'] = 'actions';
    if ($max_refund_money->compareTo(BigDecimal::zero()) > 0) {
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => '退款',
        '#button_type' => 'primary',
        '#ajax' => [
          'callback' => '::refund',
          'wrapper' => 'refund-result-wrapper',
          'prevent' => 'click',
          'method' => 'html',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Submitting refund...'),
          ],
        ],
      ];
      $form['content_one'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('The refund processing results will be displayed here after submission'),
        '#attributes' => ['id' => 'refund-result-wrapper'],
      ];
    }
    else {
      $destination = \Drupal::request()->query->get('destination');
      if (!empty($destination)) {
        $form['content_one'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('The order has been fully refunded.'),
        ];
        $form['backward'] = [
          '#type' => 'link',
          '#title' => $this->t('Backwards'),
          '#attributes' => [
            'class' => 'button',
          ],
          '#url' => Url::fromUserInput($destination),
        ];
      }
    }
    $form['#attached']['library'][] = 'wn_events/wn_events_refund_form';
    return $form;
  }

  public function refund(array &$form, FormStateInterface $form_state) {
    $result = $form_state->get('result');
    unset($result['sign']);
    $title = $this->t('Refund query submitted');
    if (isset($result['refund_state'])) {
      $state = [
        0 => $this->t('Refund initiated'),
        1 => $this->t('Refund successful'),
        2 => $this->t('Refund failed'),
      ];
      $title = $this->t('Refund status:')
          ->render() . '<strong>' . $state[(int) $result['refund_state']] . '</strong>';
    }
    $return = [];
    $return['refund_info'] = [
      '#theme' => 'status_messages',
      '#status_headings' => [
        'status' => $this->t('Status message'),
        'error' => $this->t('Error message'),
      ],
    ];
    $return['refund_info_data'] = [
      '#type' => 'details',
      '#title' => $title,
      '#prefix' => '<div id="refund-info-data">',
      '#sufix' => '</div>',
      '#open' => FALSE,
    ];
    $result_info = [
      !empty($result['code']) ? $this->t('Code:')
          ->render() . $result['code'] : '',
      !empty($result['msg']) ? $this->t('Message:')
          ->render() . $result['msg'] : '',
      !empty($result['user_id']) ? $this->t('User id:')
          ->render() . $result['user_id'] : '',
      !empty($result['order_number']) ? $this->t('Order number:')
          ->render() . $result['order_number'] : '',
      !empty($result['system_number']) ? $this->t('System number:')
          ->render() . $result['system_number'] : '',
      !empty($result['refund_number']) ? $this->t('Refund number:')
          ->render() . $result['refund_number'] : '',
      !empty($result['amount']) ? $this->t('Amount:')
          ->render() . $result['amount'] : '',
      !empty($result['total']) ? $this->t('Total:')
          ->render() . $result['total'] : '',
      !empty($result['refund_state']) ? $this->t('Refund state:')
          ->render() . $result['refund_state'] : '',
      !empty($result['refund_time']) ? $this->t('Refund time:')
          ->render() . $result['refund_time'] : '',
      !empty($result['notify_url']) ? $this->t('Notify url:')
          ->render() . $result['notify_url'] : '',
    ];
    $data = $this->t('Unified platform returns data:')
        ->render() . '<pre>' . implode(',<br>', array_filter($result_info)) . "</pre>";
    $return['refund_info_data']['data'] = ['#markup' => $data];
    if (isset($result['refund_state'])) {
      $return['refund_info']['#message_list']['status'][] = $this->t('A refund has been initiated. You can click on the "Check Refund" button to view it.');
      $url = Url::fromRoute('view.wn_refund_information_content.refund_information_page', [], [
        'query' => [
          'order_number' => $this->comment->get('field_wn_order_number')->value,
        ],
      ]);
      $return['check'] = [
        '#type' => 'link',
        '#title' => $this->t('Check Refund'),
        '#url' => $url,
        '#attributes' => [
          'class' => ['button'],
        ],
      ];
      $response = new AjaxResponse();
      $selector = '#edit-submit';
      $response->addCommand(new ReplaceCommand($selector, \Drupal::service('renderer')
        ->render($return)));
      return $response;
    }
    else {
      $return['refund_info']['#message_list']['error'][] = $this->t('Refund failed.');
    }
    return $return;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['order_number'] = trim($form_state->getValue('order_number'));
    $order['refund_number'] = trim($form_state->getValue('refund_number'));
    $order['amount'] = $form_state->getValue('amount');
    if ($order['amount'] <= 0) {
      $form_state->setError($form, $this->t("Refund number can't be zero."));
    }
    $order['reason'] = trim($form_state->getValue('reason'));
    $order['notify_url'] = trim($form_state->getValue('notify_url'));
    $sdk = \Drupal::service('wn_events_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order, 'refund');
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    else {
      $nodes = $this->nodeStorage->loadByProperties([
        'title' => $order['refund_number'],
      ]);
      if (empty($nodes)) {
        $node = Node::create([
          'type' => 'wn_refund_information',
          'title' => $order['refund_number'],
          'field_wn_order_number' => $order['order_number'],
          'field_wn_refund_time' => time(),
          'field_wn_refund_reason' => $order['reason'],
          'field_wn_events_money' => strval($order['amount']),
          'field_wn_refund_state' => YK_ORDER_REFUND_STATE_WAIT,
          'status' => TRUE,
        ]);
      }
      else {
        $node = reset($nodes);
        $node->set('field_wn_refund_time', time());
        $node->set('field_wn_refund_reason', $order['reason']);
        $node->set('field_wn_events_money', strval($order['amount']));
      }
      $node->save();
      $this->comment->set('field_wn_events_paid', YK_ORDER_STATE_REFUND_PROGRESS);
      $this->comment->save();
      $order['notify_url'] = Url::fromRoute('wn_events.refund.notify', [], [
        'absolute' => TRUE,
        'query' => [
          'nid' => $node->id(),
        ],
      ])->toString();
    }
    $result = $sdk->refund($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //In the AJAX submission of the form, after the validator passes, the submitter will execute it, and finally execute the Ajax callback
    //表单的AJAX提交中，验证器通过后提交器会执行 ，最后再执行ajax回调
    $refund_info_views_url = Url::fromRoute('view.wn_refund_information_content.refund_information_page', [], [
      'query' => [
        'order_number' => $this->comment->get('field_wn_order_number')->value,
      ],
    ])->toString();
    \Drupal::request()->query->set('destination', $refund_info_views_url);
  }

}
