<?php
/**
 * Example of merchant order query
 * 商家订单查询示例
 *
 */

namespace Drupal\wn_events\Form\payment;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class QueryForm extends FormBase {

  public function getFormId() {
    return 'wn_events_query_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['order_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order No.'),
      '#description' => $this->t('Please enter the order number you submitted in the payment presentation'),
      '#maxlength' => 32,
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Query'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::query',
        'wrapper' => 'query-result-wrapper',
        'prevent' => 'click',
        'method' => 'html',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Querying...'),
        ],
      ],
    ];
    $form['content_one'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('After clicking on the query, the results will be displayed here'),
      '#attributes' => ['id' => 'query-result-wrapper'],
    ];
    return $form;
  }

  public function query(array &$form, FormStateInterface $form_state) {
    $result = $form_state->get('result');
    unset($result['sign']);
    $title = $this->t('Queried');
    if (isset($result['order_state'])) {
      // 0 | Waiting for payment, 1 | Successful payment, 2 | Payment failed, 3 | Refund in progress, 4 | Partial refund, 5 | Full refund
      // 0|等待付款、1|成功付款、2|付款失败、3|退款中、4|部分退款、5|全部退款
      $state = [
        0 => $this->t('Waiting for payment'),
        1 => $this->t('Successful payment'),
        2 => $this->t('Payment failed'),
        3 => $this->t('Refund in progress'),
        4 => $this->t('Partial refund'),
        5 => $this->t('Full refund'),
      ];
      $title = $this->t('Order payment status:')
          ->render() . '<strong>' . $state[(int) $result['order_state']] . '</strong>';
      \Drupal::messenger()->addMessage($title);
    }

    $return = [];
    $return['order_info'] = [
      '#type' => 'details',
      '#title' => $title,
      '#open' => TRUE,
    ];
    $data = $this->t('Unified platform returns data:')
        ->render() . '<pre>' . print_r($result, TRUE) . "</pre>";
    \Drupal::messenger()->addMessage($data);
    $return['order_info']['data'] = ['#markup' => $data];
    return $return;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['order_number'] = trim($form_state->getValue('order_number'));

    $sdk = \Drupal::service('wn_events_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order, 'query');
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    $result = $sdk->query($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //In the AJAX submission of the form, after the validator passes, the submitter will execute it, and finally execute the Ajax callback
    //表单的AJAX提交中，验证器通过后提交器会执行 ，最后再执行ajax回调
  }

}
