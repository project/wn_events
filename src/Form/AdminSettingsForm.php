<?php

/**
 * wn_events configure form
 * wn_events模块配置表单
 * by:LiJiacheng
 */

namespace Drupal\wn_events\Form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminSettingsForm extends ConfigFormBase {

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_display.repository'),
    );
  }

  /**
   * Creates a AdminSettingsForm instance.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   */
  public function __construct(
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wn_events_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#sponsorConfig'] = $this->config('wn_events.settings');
    $form['show_node_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display event title'),
      '#description' => $this->t('By default, the content title will be hidden to avoid duplication with the title of the page title block. But if you have removed the page title block, please check this option to display the title of the content.'),
      '#default_value' => $form['#sponsorConfig']->get('show_node_title'),
    ];
    $display_mode = empty($form['#sponsorConfig']->get('display_mode')) ? 'default' : $form['#sponsorConfig']->get('display_mode');
    $waiting_order_clear_time = empty($form['#sponsorConfig']->get('waiting_order_clear_time')) ? 2592000 : $form['#sponsorConfig']->get('waiting_order_clear_time');
    $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle('node', 'wn_events');
    $view_modes_option = array_combine(array_keys($view_modes), array_keys($view_modes));
    $form['display_mode'] = [
      '#type' => "select",
      '#title' => $this->t("Display Mode"),
      '#description' => $this->t("Select the display mode for the event content type used for the event page."),
      '#options' => $view_modes_option,
      '#default_value' => $display_mode,
    ];
    $form['waiting_order_clear_time'] = [
      '#type' => "number",
      '#title' => $this->t("Interval"),
      '#description' => $this->t("Interval for clearing orders waiting for paid(in seconds).Default:30 days."),
      '#default_value' => $waiting_order_clear_time,
    ];
    $notice = $this->t('Set up unified payment platform interface information, and only after correct settings can payment function be used.For more details, please go to: https://pay.will-nice.com/');
    $form['notice'] = [
      '#markup' => $notice,
    ];
    $form['userID'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User id'),
      '#description' => $this->t('User ID registered on the unified payment platform'),
      '#required' => TRUE,
      '#default_value' => $form['#sponsorConfig']->get('userID'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Communication key'),
      '#description' => $this->t('The key for communication with the unified payment platform must be set synchronously with the platform, otherwise communication will not be successful'),
      '#required' => TRUE,
      '#default_value' => $form['#sponsorConfig']->get('key'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['apiOrderURL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Place order interface address'),
      '#description' => $this->t('The system will obtain the actual payment address of the user\'s order through this interface'),
      '#required' => TRUE,
      '#default_value' => $form['#sponsorConfig']->get('api.orderURL'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['apiQueryURL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query interface address'),
      '#description' => $this->t('The system will obtain the actual order status of the user through this interface'),
      '#required' => TRUE,
      '#default_value' => $form['#sponsorConfig']->get('api.queryURL'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['apiRefundURL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refund interface address'),
      '#description' => $this->t('The system will perform a refund operation through this interface'),
      '#required' => TRUE,
      '#default_value' => $form['#sponsorConfig']->get('api.refundURL'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['apiQueryRefundURL'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refund query interface address'),
      '#description' => $this->t('The system will query the refund status through this interface'),
      '#required' => TRUE,
      '#default_value' => $form['#sponsorConfig']->get('api.queryRefundURL'),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('wn_events.settings');
    if ($form_state->hasValue('display_mode')) {
      $config->set('display_mode', $form_state->getValue('display_mode'));
    }
    if ($form_state->hasValue('waiting_order_clear_time')) {
      $config->set('waiting_order_clear_time', $form_state->getValue('waiting_order_clear_time'));
    }
    $config->set('show_node_title', $form_state->getValue('show_node_title'));
    $config->set('userID', $form_state->getValue('userID'));
    $config->set('key', $form_state->getValue('key'));
    $config->set('api.orderURL', $form_state->getValue('apiOrderURL'));
    $config->set('api.queryURL', $form_state->getValue('apiQueryURL'));
    $config->set('api.refundURL', $form_state->getValue('apiRefundURL'));
    $config->set('api.queryRefundURL', $form_state->getValue('apiQueryRefundURL'));
    $config->save();
    drupal_flush_all_caches();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['wn_events.settings'];
  }

}
