<?php

/**
 * Events Form
 * 项目参与表单
 * by:LiJiacheng
 */

namespace Drupal\wn_events\Form;

use Brick\Math\BigDecimal;
use Drupal\comment\Entity\Comment;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\wn_events\WnEventsPerformance;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EventsForm extends FormBase {

  /**
   * The node storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * @var \Drupal\comment\CommentInterface
   */
  protected $comment;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('node'),
    );
  }

  /**
   * Creates a EventsForm instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage handler.
   */
  public function __construct(
    EntityStorageInterface $node_storage
  ) {
    $this->nodeStorage = $node_storage;
    $this->node = \Drupal::routeMatch()->getParameter('node');
    $this->comment = Comment::create([
      'entity_id' => $this->node->id(),
      'entity_type' => 'node',
      'field_name' => 'field_wn_events_comment',
      'comment_type' => 'wn_events_comment',
      'subject' => '',
      'comment_body' => '',
      'status' => FALSE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wn_events_wn_events_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['information'] = [
      '#type' => 'markup',
      '#markup' => $this->node->get('field_wn_payment_form_info')->value,
    ];
    $field_wn_events_amount_op = $this->node->get('field_wn_events_amount_op')
      ->getValue();
    $sponsor_amount_values = array_map(function($value) {
      return $value['value'];
    }, $field_wn_events_amount_op);
    $form['events_amount_options'] = [
      '#theme' => 'events_amount_options',
      '#items' => $sponsor_amount_values,
    ];
    $form['events_amount_options']['#attached']['library'][] = 'wn_events/events_amount_options';
    $field_wn_default_events_amount = $this->node->get('field_wn_default_events_amount')->value;
    $this->comment->set('field_wn_events_money', $field_wn_default_events_amount);
    $field_wn_hide_events_info = $this->node->get('field_wn_hide_events_info')->value;
    $this->comment->set('field_wn_events_paid', $field_wn_hide_events_info);
    $form['wn_events_form'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => $this->comment->getEntityTypeId(),
      '#bundle' => $this->comment->bundle(),
      '#form_mode' => 'wn_events_form',
      '#save_entity' => TRUE,
      '#default_value' => $this->comment,
    ];
    //The order timeout should be generated by the system and should not be entered by the user
    //订单超时时间应该由系统生成，不应由用户输入
    $form['timeout_express'] = [
      '#type' => 'value',
      '#value' => time() + 7 * 24 * 60 * 60,
    ];
    $order_number = 'wn_event_' . date('ymdHis') . mt_rand(10000000, 99999999);
    //The order number should be generated by the system according to certain rules, rather than specified by the user. For demonstration purposes, this is presented to the user
    //订单号应由系统按一定规则生成，而不是用户指定，为演示起见，这里展示给用户
    $form['order_number'] = [
      '#type' => 'value',
      '#value' => $order_number,
    ];
    $form['attach'] = [
      '#type' => 'value',
      '#value' => '',
      //The order additional data sent to the unified payment platform will be returned as is upon asynchronous notification or return
      //Any data with a length of string [1128], recommended as a JSON string
      //Internal information is not displayed to users
      //发送给统一付款平台的订单附加数据，在异步通知或返回时将原样送回
      //长度string[1,128] 任意数据，推荐为json字符串
      //内部信息不展示给用户
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pay'),
    ];
    $form['backwards'] = [
      '#type' => 'submit',
      '#value' => $this->t('Backwards'),
      '#submit' => ['::backwards'],
      '#limit_validation_errors' => [],
    ];
    $can_custom_amount = $this->node->get('field_can_custom_amount')->value;
    if (!$can_custom_amount) {
      $form['#attached']['library'][] = 'wn_events/wn_events_form_disable_custom_amount';
    }
    return $form;
  }

  public function backwards(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.node.canonical', [
      'node' => $this->node->id(),
    ], []);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $total_money = trim($form_state->getValue('wn_events_form')['field_wn_events_money'][0]['value']);
    //Allow custom amount input
    //是否允许输入自定义金额
    $can_custom_amount = $this->node->get('field_can_custom_amount')->value;
    if (!$can_custom_amount) {
      $wn_events_amount_op = $this->node->get('field_wn_events_amount_op')
        ->getValue();
      $new_wn_events_amount_op = array_map(function($value) {
        return $value['value'];
      }, $wn_events_amount_op);
      if (!in_array($total_money, $new_wn_events_amount_op)) {
        $form_state->setError($form, t('The payment amount is invalid.'));
      }
    }

    if (!is_numeric($total_money)) {
      $form_state->setError($form, t('The payment amount can only be entered in numbers.'));
    }
    else {
      $description = $this->node->label();
      $description = trim($description);
      $description = str_replace(' ', '_', $description);
      $pattern = '/[\x{4e00}-\x{9fa5}a-zA-Z0-9_]/u';
      preg_match_all($pattern, $description, $result);
      $description = join('', $result[0]);
      $description = substr($description, 0, 127);
      $order['description'] = $description;
      $order['total'] = BigDecimal::of($total_money)->toFloat();
      $order['timeout_express'] = $form_state->getValue('timeout_express');
      $order['order_number'] = trim($form_state->getValue('order_number'));
      $order['attach'] = trim($form_state->getValue('attach'));
      $order['phone'] = trim($form_state->getValue('wn_events_form')['field_wn_events_phone'][0]['value']);
      //After payment, jump back to the absolute URL address of this system. You should process the subsequent business logic after payment at this address or in asynchronous notifications
      //String [1256] must be a directly accessible URL absolute address
      //Internal information is not displayed to users
      //付款后跳回本系统的绝对URL地址，你应该在该地址或异步通知中处理付款后的后续业务逻辑
      //string[1,256] 必须为直接可访问的URL绝对地址
      //内部信息不展示给用户
      $route_parameters = ['orderNumber' => $order['order_number'],];
      $options = ['absolute' => TRUE,];
      $return_url = new Url('wn_events.process', $route_parameters, $options);
      $order['return_url'] = $return_url->toString(FALSE);
      //Asynchronous notification link for payment status, which will be used by the unified cashier platform to send a message to this system indicating whether the payment was successful
      //You should handle order change logic at this address
      //String [1256] must be a directly accessible URL absolute address
      //Internal information is not displayed to users
      //付款状态异步通知链接，统一收银平台将采用该链接向本系统发送付款是否成功的消息
      //在该地址中你应该处理订单变更逻辑
      //string[1,256] 必须为直接可访问的URL绝对地址
      //内部信息不展示给用户
      $route_parameters = ['orderNumber' => $order['order_number'],];
      $options = ['absolute' => TRUE,];
      $notify_url = new Url('wn_events.notify', $route_parameters, $options);
      $order['notify_url'] = $notify_url->toString(FALSE);
      $sdk = \Drupal::service('wn_events_paysdk.pay');
      $verifyResult = $sdk->verifyParameters($order);
      if ($verifyResult !== TRUE) {
        $form_state->setError($form, $verifyResult);
      }
      $result = $sdk->order($order);
      if ($result['code'] >= 4000) {
        $form_state->setError($form, $result['msg']);
      }
      $form['#order'] = $order;
      $form_state->set('result', $result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->comment->set('field_wn_events_time', time());
    $this->comment->set('field_wn_order_number', $form['#order']['order_number']);
    $this->comment->set('subject', $form['#order']['order_number']);
    $this->comment->save();
    //Directly jump to the unified payment platform and select the payment interface
    //直接跳转到统一支付平台选择付款界面
    $result = $form_state->get('result');
    $form_state->setResponse(new TrustedRedirectResponse($result['url']));
  }

}
