<?php

/**
 * PerformanceForm
 * 项目参与表单
 * by:LiJiacheng
 */

namespace Drupal\wn_events\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wn_events\WnEventsPerformance;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PerformanceForm extends FormBase {

  /**
   * The node storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * @var \Drupal\comment\CommentInterface
   */
  protected $comment;

  /**
   * @var \Drupal\node\NodeInterface
   */
  protected $performance;

  /**
   * @var string
   */
  protected $phone;

  /**
   * @var string
   */
  protected $wn_events_certificate_number;

  /**
   * @var string
   */
  protected $from_cert;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('node'),
    );
  }

  /**
   * Creates a EventsForm instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage handler.
   */
  public function __construct(
    EntityStorageInterface $node_storage
  ) {
    $this->nodeStorage = $node_storage;
    $this->node = \Drupal::routeMatch()->getParameter('node');
    $this->comment = \Drupal::routeMatch()->getParameter('comment');
    $this->performance = WnEventsPerformance::getCommentPerformanceNode($this->comment);
    $this->phone = \Drupal::request()->query->get('phone');
    $this->wn_events_certificate_number = \Drupal::request()->query->get('wn_events_certificate_number');
    $this->from_cert = \Drupal::request()->query->get('from_cert');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wn_events_performance_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['cert_number'] = [
      '#type' => 'inline_template',
      '#template' => '<div id="cert-num">凭证编号：{{ value }}</div>',
      '#context' => ['value' => WnEventsPerformance::getCertificateNumber($this->comment)],
    ];
    $field_time = $this->performance->get('field_time')->getValue();
    $record_list = array_map(function($value) {
      return $value['value'];
    }, $field_time);
    $form['record_list'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Performance history'),
      '#items' => $record_list,
      '#attributes' => ['class' => 'record-list'],
      '#wrapper_attributes' => ['class' => 'container'],
    ];
    $state = $this->performance->get('field_state')
      ->isEmpty() ? 0 : $this->performance->get('field_state')->value;
    $form['state'] = [
      '#type' => 'select',
      '#options' => WnEventsPerformance::getPerformanceStateOptions(),
      '#title' => $this->t('State'),
      '#default_value' => $state,
      '#attributes' => ['class' => ['notes']],
    ];
    $message = $this->performance->get('field_message')->value;
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notes'),
      '#default_value' => $message,
      '#attributes' => ['class' => ['notes']],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    $form['backwards'] = [
      '#type' => 'submit',
      '#value' => $this->t('Backwards'),
      '#submit' => ['::backwards'],
      '#limit_validation_errors' => [],
    ];
    $form['#attached']['library'][] = 'wn_events/wn_events_performance_form';
    return $form;
  }

  public function backwards(array &$form, FormStateInterface $form_state) {
    $this->_performanceFormRedirect($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current_state = $this->performance->get('field_state')->value;
    $state = $form_state->getValue('state');
    if ($current_state != $state) {
      $message = $form_state->getValue('message');
      $field_state_value = $this->performance->get('field_time')->getValue();
      $field_state_value[] = WnEventsPerformance::getPerformanceText(intval($state));
      $this->performance->set('field_related_cert', $this->comment->id());
      $this->performance->set('field_message', $message);
      $this->performance->set('field_time', $field_state_value);
      $this->performance->set('field_state', $state);
      $this->performance->save();
    }
    $this->_performanceFormRedirect($form, $form_state);
  }

  public function _performanceFormRedirect(array &$form, FormStateInterface $form_state) {
    if (!empty($this->phone) || !empty($this->wn_events_certificate_number)) {
      if (!empty($this->from_cert) && $this->from_cert == 1) {
        $form_state->setRedirect('wn_events.certificate', [
          'node' => $this->node->id(),
          'comment' => $this->comment->id(),
        ], [
          'query' => [
            'phone' => $this->phone,
            'wn_events_certificate_number' => $this->wn_events_certificate_number,
          ],
        ]);
      }
      else {
        $form_state->setRedirect('view.wn_events_certificate_query.page_1', [
          'node' => $this->node->id(),
        ], [
          'query' => [
            'phone' => $this->phone,
            'wn_events_certificate_number' => $this->wn_events_certificate_number,
          ],
        ]);
      }
    }
    else {
      $form_state->setRedirect('wn_events.certificate', [
        'node' => $this->node->id(),
        'comment' => $this->comment->id(),
      ], []);
    }
  }

}
