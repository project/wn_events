<?php

namespace Drupal\wn_events\Plugin\views\filter;

use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Filter by certificate number.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("wn_events_certificate_number")
 */
class WnEventsCertificateNumber extends StringFilter {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $table = array_key_first($query->tables);
    $node = \Drupal::routeMatch()->getParameter('node');
    if (!empty($node) && $node instanceof NodeInterface) {
      $nid = $node->id();
      $query->addWhere('AND', $table . '.entity_id', $nid, '=');
    }
    $query_number = \Drupal::request()->query->get('wn_events_certificate_number');
    $cid = substr($query_number, 6);
    $year = substr($query_number, 0, 2);
    $month = substr($query_number, 2, 2);
    $date = substr($query_number, 4, 2);
    $time = $year . "-" . $month . "-" . $date;
    $start_time = strtotime($time);
    $end_time = $start_time + 86400;
    $query->addWhere('AND', $table . '.cid', $cid, '=');
    $query->addWhere('AND', $table . '.created', $start_time, '>=');
    $query->addWhere('AND', $table . '.created', $end_time, '<');
  }

}
