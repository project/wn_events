<?php

/**
 * @file
 * Definition of
 *   Drupal\wn_events\Plugin\views\field\WnEventsPerfomanceOperation
 */

namespace Drupal\wn_events\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\views\Annotation\ViewsField;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\wn_events\WnEventsPerformance;

/**
 * Field handler class group views operation.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("wn_events_performance_operation")
 */
class WnEventsPerfomanceOperation extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options
   *
   * @return array
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    $node = $entity->getCommentedEntity();
    $link = NULL;
    if ($node->get('field_is_need_perform')->value) {
      $phone = \Drupal::request()->query->get('phone');
      $wn_events_certificate_number = \Drupal::request()->query->get('wn_events_certificate_number');
      if (WnEventsPerformance::hasPerformanceRole()) {
        $link = Link::fromTextAndUrl($this->t('Perform'), Url::fromRoute('wn_events.performance_form', [
          'node' => $node->id(),
          'comment' => $entity->id(),
        ], [
          'query' => [
            'phone' => $phone,
            'wn_events_certificate_number' => $wn_events_certificate_number,
          ],
        ]))->toString();
      }
    }
    return $link;
  }

}
