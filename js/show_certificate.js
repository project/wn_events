(function (Drupal, drupalSettings) {
  /*
    The certificate details page JS allows the width and height of the certificate display section
    to be equivalent to the actual uploaded certificate background image, in order to ensure that
    the final downloaded image is consistent with the effect seen on the webpage.
  */
  /*
    凭证详情页面JS，让凭证显示部分的宽高与实际上传的凭证背景图片相等，目的是为了最终下载的图片与网页中所见的效果是一致的。
  */
  window.onload = function () {
    once('show_certificate', '#wn-events-overlay').forEach(function (element) {
      // set div container width and height
      // 设置容器宽高
      const certificate_background = document.getElementById('certificate-background');
      const wn_events_certificate = document.getElementById('wn-events-certificate');
      element.style.width = certificate_background.offsetWidth + 'px !important';
      element.style.height = certificate_background.offsetHeight + 'px !important';
      wn_events_certificate.style.width = certificate_background.offsetWidth + 'px !important';
      wn_events_certificate.style.height = certificate_background.offsetHeight + 'px !important';

      // Download certificate
      // 下载凭证
      const download_button = document.getElementById('download-certificate');
      download_button.addEventListener('click', function () {
        download_button.textContent = Drupal.t('Please wait...');
        //Determine if it is a weixin browser
        //判断是否为微信浏览器或
        let ua = window.navigator.userAgent.toLowerCase();
        let isWeixin = /MicroMessenger/i.test(ua);
        let isSafari = /^((?!chrome|android).)*safari/i.test(ua);
        if (isWeixin) {
          longPressDownload();
        } else if (isSafari) {
          svgDownload('certificate.png');
        } else {
          directDownload('certificate.png');
        }
        download_button.textContent = Drupal.t('Download');
      });

      const container_ele = document.getElementById('wn-events-cert-container');
      const scale_ele = document.getElementById('wn-events-scale');
      scale_ele.style.transform = 'scale(' + container_ele.offsetWidth / scale_ele.offsetWidth + ')';
      scale_ele.style.webkitTransform = 'scale(' + container_ele.offsetWidth / scale_ele.offsetWidth + ')';
      scale_ele.style.transformOrigin = 'top left';
      container_ele.style.height = scale_ele.offsetHeight * (container_ele.offsetWidth / scale_ele.offsetWidth) + 'px';
    });
  };

  function longPressDownload() {
    htmlToImage.toJpeg(document.getElementById('wn-events-overlay'), { quality: 1 })
      .then(function (dataUrl) {
        let alertBox = document.createElement('div'); // 创建一个新的<div>元素作为弹出框容器
        alertBox.setAttribute('id', 'cert-image');
        let pointer_hint_text = Drupal.t('Please long press the image to save the certificate.');
        alertBox.innerHTML = '<span id="cert-image-close">x</span><img src="' + dataUrl + '"><div id="cert-save-tip"><img alt="certificate.jpeg" src="' + drupalSettings.module_path + '/images/icons-natural-user-interface-100.png"><span id="tip-text">' + pointer_hint_text + '</span></div>';
        document.body.appendChild(alertBox);
        document.getElementById('cert-image-close').addEventListener('click', function () {
          document.getElementById('cert-image').remove();
        });
      });
  }

  function directDownload(name) {
    htmlToImage.toBlob(document.getElementById('wn-events-overlay'))
      .then(function (blob) {
        if (window.saveAs) {
          window.saveAs(blob, name);
        } else {
          FileSaver.saveAs(blob, name);
        }
      });
  }

  function svgDownload(name){
    htmlToImage.toSvg(document.getElementById('wn-events-overlay'), { quality: 1 })
      .then(function (dataUrl) {
        let link = document.createElement('a');
        let body = document.querySelector('body');
        link.style.display = 'none';
        link.href = dataUrl;
        link.download = name;
        body.appendChild(link);
        link.click();
        body.removeChild(link);
      });
  }
}(Drupal, drupalSettings));
