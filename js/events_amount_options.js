(function (Drupal) {
  // On the events form page, click on the amount option to change the events amount.
  // 付款表单页面，点击金额选项后更改付款金额。
  Drupal.behaviors.events_amount_options = {
    attach: function (context, settings) {
      once('events_amount_options', '#events-amount-items .card').forEach(function (element) {
        element.addEventListener('click', function(){
          let money;
          money = element.querySelector('.number-of-money').textContent;
          document.getElementById('edit-wn-events-form-field-wn-events-money-0-value').value = money;
        })
      });
    }
  };
}(Drupal));
