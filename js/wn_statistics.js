(function (Drupal) {
  // Events list page, data statistics growth animation.
  // 项目列表页面，数据统计增长动画。
  Drupal.behaviors.number_increase = {
    attach: function (context, settings) {
      once('number_increase', 'div#wn-events-statistics span .number.float').forEach(function (element) {
        let end = element.textContent;
        let i = 0;
        let floatInterval;
        if(i < end){
          floatInterval = setInterval(function(){
            // Set the dynamic number for each increase, which can be adjusted.
            // 设置每次增加的动态数字，可调整
            i += end/10;
            if(i > end) {
              // Clear the time of setInterval
              // 清除setInterval的time
              clearInterval(floatInterval);
              // This assignment is to avoid discrepancies between the data after the last addition and the actual data
              // 此赋值是为了避免最后一次增加过后的数据和真实数据不同
              element.innerHTML = end.toLocaleString();
              i = 0;
            } else {
              element.innerHTML = i.toLocaleString();
            }
          }, 100);
          // Speed control of data jump
          // 数据跳转的速度控制
        }
      });
      once('number_increase', 'div#wn-events-statistics span .number.integer').forEach(function (element) {
        let end = element.textContent;
        let i = 0;
        let integerInterval;
        if(i < end){
          integerInterval = setInterval(function(){
            // Set the dynamic number for each increase, which can be adjusted.
            // 设置每次增加的动态数字，可调整
            i += end/10;
            if(i > end) {
              // Clear the time of setInterval
              // 清除setInterval的time
              clearInterval(integerInterval);
              // This assignment is to avoid discrepancies between the data after the last addition and the actual data
              // 此赋值是为了避免最后一次增加过后的数据和真实数据不同
              element.innerHTML = end.toLocaleString();
              i = 0;
            } else {
              element.innerHTML = i.toLocaleString();
            }
          }, 100);
          // 数据跳转的速度控制
        }
      });
    }
  };
}(Drupal));
