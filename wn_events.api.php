<?php

/**
 * @file
 * Hooks specific to the Will Nice Events module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 *
 * The hook for successful events payment facilitates other modules to
 * execute actions triggered after successful events,such as opening read
 * permissions for a certain article.
 * 付款成功的钩子，便于其他模块执行为项目支付成功后触发的动作，比如开通某文章的读取权限。
 *
 * @code
 * $data = [
 *   'content_id' => Events collection content ID,
 *   'comment_id' => Comment ID(Participants Order),
 *   'user' => [
 *      'name' => Participants order owner real name,
 *      'wechat' => Participants order owner WeChat number,
 *      'phone' => Participants order owner contact phone,
 *   ],
 *   'amount' => The events money amount user paid,
 *   'time' => The events submitted time,
 * ];
 * \Drupal::database()->insert('node_access')->fields($record)->execute();
 *
 * @param array $data
 *
 * @return void
 *
 */
function hook_wn_events_payment_success(array &$data) {
  $cid = $data['comment_id'];
  if ($event_order = \Drupal\comment\Entity\Comment::load($cid)) {
    $status = $event_order->get('field_wn_events_paid')->value;
    if(in_array($status, [YK_ORDER_STATE_SUCCESS, YK_ORDER_STATE_REFUND_PROGRESS, YK_ORDER_STATE_REFUND_PART])){
      $user = \Drupal::currentUser();
      // Example:Opening this user's read permissions for a certain article.
      // ...
      // 例子：为该用户开通读取某篇文章的权限。
      // ...
    }
  }
}

/**
 * Hook for successful events refund, used for processing after refund.
 * 退款成功的钩子，用于退款后的处理。
 *
 * @code
 * $data = [
 *   'content_id' => Participants collection content ID,
 *   'comment_id' => Comment ID(Participants Order),
 *   'user' => [
 *      'name' => Participants order owner real name,
 *      'wechat' => Participants order owner WeChat number,
 *      'phone' => Participants order owner contact phone,
 *   ],
 *   'amount' => The participants amount paid order,
 *   'time' => The participants submitted time,
 * ];
 * \Drupal::database()->insert('node_access')->fields($record)->execute();
 *
 * @param array $data
 */
function hook_wn_events_refund_success(array &$data) {
  // Processing after refund.
  // ...
  // 退款后的处理。
  // ...
}

/**
 * @} End of "addtogroup hooks".
 */
