<?php

/**
 * Implements hook_views_data().
 */
function wn_events_views_data() {
  $data['views']['wn_events_statistics_area'] = [
    'title' => 'Will Nice Events Statistics',
    'help' => 'Provides a events statistics area.',
    'area' => [
      'id' => 'wn_events_statistics_area',
    ],
  ];
  $data['comment']['wn_events_certificate_number'] = [
    'title' => t('Certificate Number'),
    'filter' => [
      'title' => t('Events Certificate Number - wn_events Filter'),
      'field' => 'field_wn_events_comment',
      'id' => 'wn_events_certificate_number',
    ],
  ];
  $data['comment']['wn_events_performance_operation'] = [
    'title' => t('Events Performance Operation'),
    'field' => [
      'title' => t('Events Performance Operation'),
      'id' => 'wn_events_performance_operation',
    ],
  ];
  return $data;
}
